--SELECT *, ROUND(CONVERT(FLOAT,R)/CONVERT(FLOAT,B)*100,2) SR FROM
--(SELECT batsman, 
--	(SELECT SUM(runs) FROM match WHERE batsman = P.batsman AND type != 'wide' AND type!='byes') R,
--	(SELECT COUNT(ball) FROM match WHERE batsman = P.batsman AND type != 'wide') B,
--	(SELECT COUNT(runs) FROM match WHERE batsman = P.batsman AND runs=4 AND type != 'wide') fours,
--	(SELECT COUNT(runs) FROM match WHERE batsman = P.batsman AND runs=6 AND type != 'wide') sixes
--FROM
--(SELECT batsman
--FROM match
--WHERE innings = 1
--GROUP BY batsman) P) T

--SELECT *, ROUND(CONVERT(FLOAT,R)/CONVERT(FLOAT,B)*100,2) SR FROM
--(SELECT batsman, 
--	(SELECT SUM(runs) FROM match WHERE batsman = P.batsman AND type != 'wide' AND type!='leg bye' AND type!='no ball') R,
--	(SELECT COUNT(ball) FROM match WHERE batsman = P.batsman AND type != 'wide') B,
--	(SELECT COUNT(runs) FROM match WHERE batsman = P.batsman AND runs=4 AND type != 'wide') fours,
--	(SELECT COUNT(runs) FROM match WHERE batsman = P.batsman AND runs=6 AND type != 'wide') sixes
--FROM
--(SELECT batsman
--FROM match
--WHERE innings = 2
--GROUP BY batsman) P) T

SELECT *, ROUND(CONVERT(FLOAT,R)/CONVERT(FLOAT,O),2) ECON FROM
(SELECT bowler, 
	(SELECT COUNT(ball)/6 FROM match WHERE bowler = P.bowler AND type != 'wide') O,
	(SELECT SUM(runs) FROM match WHERE bowler = P.bowler AND type != 'byes') R,
	(SELECT COUNT(runs) FROM match WHERE bowler = P.bowler AND type='dot') zeros,
	(SELECT COUNT(runs) FROM match WHERE bowler = P.bowler AND runs=4) fours,
	(SELECT COUNT(runs) FROM match WHERE bowler = P.bowler AND runs=6) sixes,
	(SELECT COUNT(ball) FROM match WHERE bowler = P.bowler AND type = 'out') W,
	(SELECT COUNT(ball) FROM match WHERE bowler = P.bowler AND type = 'wide') WD,
	(SELECT COUNT(ball) FROM match WHERE bowler = P.bowler AND type = 'no ball') NB
FROM
(SELECT bowler
FROM match
WHERE innings = 1
GROUP BY bowler) P) T


SELECT *, ROUND(CONVERT(FLOAT,R)/CONVERT(FLOAT,o),2) ECON FROM
(SELECT bowler, 
	(SELECT COUNT(ball)/6 FROM match WHERE bowler = P.bowler AND type != 'wide') O,
	(SELECT SUM(runs) FROM match WHERE bowler = P.bowler AND type != 'byes') R,
	(SELECT COUNT(runs) FROM match WHERE bowler = P.bowler AND type='dot') zeros,
	(SELECT COUNT(runs) FROM match WHERE bowler = P.bowler AND runs=4) fours,
	(SELECT COUNT(runs) FROM match WHERE bowler = P.bowler AND runs=6) sixes,
	(SELECT COUNT(ball) FROM match WHERE bowler = P.bowler AND type = 'out') W,
	(SELECT COUNT(ball) FROM match WHERE bowler = P.bowler AND type = 'wide') WD,
	(SELECT COUNT(ball) FROM match WHERE bowler = P.bowler AND type = 'no ball') NB
FROM
(SELECT bowler
FROM match
WHERE innings = 2
GROUP BY bowler) P) T
